#include <QtCore/QFile>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <data/Shared_data.h>
#include <QtCore/QTime>
#include <QtWidgets/QTimeEdit>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "algorithms/MAS.h"
#include <QGraphicsScene>
#include <QStatusBar>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    _mas = new MAS();
    _data = new Shared_data();
    _mot = new MorphologicalTransformation();


    // Default MAT deltas are 4 and 4
    _mas->set_delta_distance(0);
    _mas->set_delta_skeleton(0);

    // Default golay part is even numbers
    _mot->set_golay(0);

    // Medial axis transformation signals
    QObject::connect(_mas, SIGNAL(log_signal(QString)), this, SLOT(log(QString)));
    QObject::connect(_mas, SIGNAL(error_signal(QString)), this, SLOT(error(QString)));
    QObject::connect(_mas, SIGNAL(thread_progress_signal(int)), this, SLOT(progress_slot(int)));
    QObject::connect(_mas, SIGNAL(image_signal(QImage)), this, SLOT(show_image(QImage)));

    // Shared data signals  for logging
    QObject::connect(_data, SIGNAL(log_signal(QString)), this, SLOT(log(QString)));
    QObject::connect(_data, SIGNAL(error_signal(QString)), this, SLOT(error(QString)));

    // Morphological transformation signals
    QObject::connect(_mot, SIGNAL(image_signal(QImage)), this, SLOT(show_image(QImage)));
    QObject::connect(_mot, SIGNAL(error_signal(QString)), this, SLOT(error(QString)));
    QObject::connect(_mot, SIGNAL(thread_progress_signal(int)), this, SLOT(progress_slot(int)));
    QObject::connect(_mot, SIGNAL(log_signal(QString)), this, SLOT(log(QString)));

    // UI signals
    QObject::connect(ui->tresholdSelect, SIGNAL(valueChanged(int)), this, SLOT(set_threshold(int)));
    QObject::connect(ui->iterBox, SIGNAL(valueChanged(int)), this, SLOT(set_max_iters(int)));
    QObject::connect(ui->deltaBox, SIGNAL(currentIndexChanged(int)), this, SLOT(set_delta_distance_slot(int)));
    QObject::connect(ui->deltaBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(set_delta_skeleton_slot(int)));
    QObject::connect(ui->golay_box, SIGNAL(currentIndexChanged(int)), this, SLOT(set_golay_slot(int)));
    QObject::connect(ui->fgBox, SIGNAL(currentIndexChanged(int)), this, SLOT(set_fg_color(int)));

    // So result preview can be openned just once
    _result_preview_open = false;

    // Disable resizing
    this->setFixedSize(size());
}

MainWindow::~MainWindow() {
    delete ui;
}

/**
 * Logs message into console in main window
 *
 * @brief MainWindow::log
 * @param string
 */
void MainWindow::log(QString string) {
    QTime system_time = QTime::currentTime();
    ui->consoleText->append(system_time.toString("[hh:mm:ss]: ") + string);
}

/**
 * Logs error into console in main window
 *
 * @brief MainWindow::error
 * @param error
 */
void MainWindow::error(QString error) {
    QTime system_time = QTime::currentTime();
    ui->consoleText->append("<span style='color:red;'>" + system_time.toString("[hh:mm:ss]: ") + "<b>CHYBA! </b>" + error + "</span>");
}

/**
 * Image loading
 *
 * @brief MainWindow::on_actionOpen_triggered
 */
void MainWindow::on_actionOpen_triggered() {
    QString fileName = QFileDialog::getOpenFileName(this, "Open the file");

    QImage img(fileName);
    if (img.isNull()) {
        return;
    }

    // Load source image inte graphicsView
    auto scene = new QGraphicsScene(this);
    QPixmap pixmap(QPixmap::fromImage(img));
    QGraphicsPixmapItem item(pixmap);
    scene->addPixmap(pixmap);
    ui->graphicsView->setScene(scene);

    _data->set_image_file(fileName);
    log("Načten obrázek: " + fileName);
}

/**
 * Starts selected algorithm computation thread
 *
 * @brief MainWindow::on_btnSkelet_clicked
 */
void MainWindow::on_btnSkelet_clicked() {

    if(_data->get_image().isNull()){
        error(QString("Nejdříve vyberte obrázek."));
        return;
    }
    // Medial Axis Skelet
    if(ui->methodBox->currentIndex() == 0){
        _mas->set_shared_data(_data);
        _mas->start();
    }
    // Morphological transforamtion
    else{
        _mot->set_shared_data(_data);
        _mot->start();
    }
}

/**
 * Saves computed image into file
 *
 * @brief MainWindow::on_btnSave_clicked
 */
void MainWindow::on_btnSave_clicked() {
    QString file_name = QFileDialog::getSaveFileName(this, "Save the file");

    if(file_name.isNull()){
        log(QString("Obrázek nebyl uložen!"));
        return;
    }

    QImage image;
    if(ui->methodBox->currentIndex()== 0)
        image = _mas->get_image();
    else
        image = _mot->get_image();

    if (image.isNull()){
        error(QString("Nastala chyba, obrázek je nulový."));
        return;
    }

    image.save(file_name + QString(".png"), "png");
    log(QString("Obrázek byl uložen do: ") + QString(file_name) + QString(".png"));
}

/**
 * Stops running thread
 *
 * @brief MainWindow::on_btnStop_clicked
 */
void MainWindow::on_btnStop_clicked(){
    // Medial Axis Skelet
    if(ui->methodBox->currentIndex() == 0){
        _mas->stop_thread();
    }else{
        // Morphological
        _mot->stop_thread();
    }
}

/**
 * Opens new window with result image
 *
 * @brief MainWindow::on_btnMaxResult_clicked
 */
void MainWindow::on_btnMaxResult_clicked(){
    if(!_result_preview_open){
        _pic_preview = new PicturePreview();
        if(ui->methodBox->currentIndex() == 0)
            _pic_preview->set_image(_mas->get_image());
        else
            _pic_preview->set_image(_mot->get_image());
        _pic_preview->show();

        QObject::connect(_pic_preview, SIGNAL(on_close_signal()), this, SLOT(result_preview_slot()));

        _result_preview_open = true;
    }
}

void MainWindow::set_threshold(int threshold) {
    if (_data != nullptr) {
        _data->set_threshold(threshold);
    }
}

void MainWindow::set_max_iters(int max_iters){
    if(_data != nullptr){
        _data->set_max_iters(max_iters);
    }
}

/**
 * Shows result image in main window graphics view
 *
 * @brief MainWindow::show_image
 * @param image
 */
void MainWindow::show_image(QImage image) {
    if(ui->graphicsView_2->scene() != nullptr)
        delete ui->graphicsView_2->scene();

    auto scene = new QGraphicsScene(this);
    QPixmap pixmap(QPixmap::fromImage(image));
    QGraphicsPixmapItem item(pixmap);
    scene->addPixmap(pixmap);
    ui->graphicsView_2->setScene(scene);
}

void MainWindow::progress_slot(int progress){
    ui->progressBar->setValue(progress);
}

void MainWindow::set_delta_distance_slot(int delta){
    _mas->set_delta_distance(delta);
}

void MainWindow::set_delta_skeleton_slot(int delta){
    _mas->set_delta_skeleton(delta);
}

void MainWindow::set_golay_slot(int golay){
    _mot->set_golay(golay);
}

void MainWindow::set_fg_color(int color){
    if(_data != nullptr){
        _data->set_fg_color(color);
    }
}

void MainWindow::result_preview_slot(){
    _result_preview_open = false;
    delete(_pic_preview);
}
