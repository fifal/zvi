#include "MorphThread.h"

MorphThread::MorphThread(Shared_data* data)
{
    _data = data;
}

/**
 * Sets which part of alphabet to use
 *
 * @brief MorphThread::set_golay
 * @param golay
 */
void MorphThread::set_golay(int golay){
    _golay = golay;
}

/**
 * Starts thread
 *
 * @brief MorphThread::run
 */
void MorphThread::run(){
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    auto image_size = _data->get_image_width() * _data->get_image_height();
    auto max_iters = _data->get_max_iters();

    emit log_signal(QString("Spuštěno výpočetní vlákno morfologické skeletizace."));
    emit log_signal(QString("Používá se práh: ")+ QString::number(_data->get_threshold()));

    auto a = new long[image_size];
    memcpy(a, _data->get_image_matrix(), sizeof(long) * image_size);

    auto iter = new long[image_size];
    memcpy(iter, a, sizeof(long) * image_size);

    auto count = 0;
    while(true){
        // Even masks
        if(_golay==0){
            // for each mask do thinning with that mask
            for(int m = 0; m < 8; m++){
                long* prev_iter = iter;
                iter = thinning(iter, _masks_even[m], _masks_even[m+1]);
                emit update_image_signal(iter);
                delete(prev_iter);
            }
        }
        // Odd masks
        else if(_golay==1){
            for(int m = 0; m < 8; m++){
                long* prev_iter = iter;
                iter = thinning(iter, _masks_odd[m], _masks_odd[m+1]);
                emit update_image_signal(iter);
                delete(prev_iter);
            }
        }
        // All masks
        else{
            for(int m = 0; m < 16; m+=2){
                long* prev_iter = iter;
                iter = thinning(iter, _masks_all[m], _masks_all[m+1]);
                emit update_image_signal(iter);
                delete(prev_iter);
            }
        }

        // If matrix are same or max iters -> end cycle
        if(Matrix::are_equal(a, iter, image_width, image_height) || count >= max_iters){
            break;
        }

        // Update progressbar
        if(count%10 ==0){
            emit progress_signal(count);
        }

        memcpy(a, iter, sizeof(long) * image_size);
        count++;
    }

    emit log_signal("Výpočet dokončen. Výsledný počet iterací: " + QString::number(count));
    emit finished_signal(a);
}

long* MorphThread::erosion(long *a, uint16_t mask){
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    auto image_size = image_width * image_height;

    long* result = new long[image_size];

    for(int i = 0; i < image_width; i++){
        for(int j = 0; j < image_height; j++){
            auto surrounding = get_surrounding(j, i, a);
            if((surrounding & mask) == mask){
                Matrix::set_on_index(j,i,1,result, image_width);
            }else{
                Matrix::set_on_index(j,i,0,result, image_width);
            }
        }
    }
    return result;
}

/**
 * Returns complement 1 - value, 1 => 0, 0 => 1
 *
 * @brief MorphThread::complement
 * @param a
 * @return
 */
long* MorphThread::complement(long *a){
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    auto image_size = image_width * image_height;

    long* result = new long[image_size];

    for(int i = 0; i < image_size; i++){
        auto value = a[i];
        result[i] = 1 - value;
    }

    return result;
}

/**
 * Returns intersection a == b => returns a, else 0
 *
 * @brief MorphThread::intersection
 * @param a
 * @param b
 * @return
 */
long* MorphThread::intersection(long *a, long *b){
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    auto image_size = image_width * image_height;

    long* result = new long[image_size];

    for(int i = 0; i < image_size; i++){
        if(a[i] == b[i]){
            result[i] = a[i];
        }
        else{
            result[i] = 0;
        }
    }
    return result;
}

/**
 * Returns thinning by calling erosion, intersection and complement operations
 *
 * @brief MorphThread::thinning
 * @param a
 * @param mask
 * @param mask2
 * @return
 */
long* MorphThread::thinning(long *a, uint16_t mask, uint16_t mask2){
    // Thinning = A inters ((A eros1 mask) inters1 (A^c eros2 mask2))^c
    auto a_eros_mask = erosion(a, mask);
    auto a_c = complement(a);
    auto a_c_eros_mask_2 = erosion(a_c, mask2);
    auto inters1 = intersection(a_eros_mask, a_c_eros_mask_2);
    auto inters1_c = complement(inters1);
    auto result = intersection(a, inters1_c);

    delete(a_eros_mask);
    delete(a_c);
    delete(a_c_eros_mask_2);
    delete(inters1);
    delete(inters1_c);
    return result;
}

/**
 * Returns surrounding for (i,j) as uin16_t
 *
 * 9 8 7
 * 6 5 4
 * 3 2 1
 *
 * @brief MorphThread::get_surrounding
 * @param i
 * @param j
 * @param matrix
 * @return
 */
uint16_t MorphThread::get_surrounding(int i, int j, const long* matrix){
    auto image_width = _data->get_image_width();
    auto image_height = _data->get_image_height();
    uint16_t okoli = 0;

    long mid = Matrix::get_on_index(i, j, matrix, image_width);

    long up = (j-1) < 0 ? 0 : Matrix::get_on_index(i, j-1, matrix, image_width);
    long up_right = (i+1) >= image_height || (j - 1) < 0 ? 0 : Matrix::get_on_index(i + 1, j - 1, matrix, image_width);
    long up_left = (i-1) < 0|| (j-1) < 0 ? 0 : Matrix::get_on_index(i - 1, j - 1, matrix, image_width);
    long down =(j+1) >= image_width ? 0 : Matrix::get_on_index(i, j + 1, matrix, image_width);
    long down_right = (i+1) >= image_height || (j+1) >= image_width ? 0 :Matrix::get_on_index(i + 1, j + 1, matrix, image_width);
    long down_left = (i-1)<0 || (j+1) >= image_width ? 0 : Matrix::get_on_index(i - 1, j + 1, matrix, image_width);
    long left = (i-1) < 0 ? 0 :Matrix::get_on_index(i-1, j, matrix, image_width);
    long right = (i+1) >= image_height ? 0 : Matrix::get_on_index(i + 1, j, matrix, image_width);

    okoli |= up_left << 8;
    okoli |= up << 7;
    okoli |= up_right << 6;
    okoli |= left << 5;
    okoli |= mid << 4;
    okoli |= right << 3;
    okoli |= down_left << 2;
    okoli |= down << 1;
    okoli |= down_right;

    return okoli;
}
