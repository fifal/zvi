#include "MorphologicalTransformation.h"

MorphologicalTransformation::MorphologicalTransformation(){
    _golay = 0;
    _thread = nullptr;
}

void MorphologicalTransformation::set_shared_data(Shared_data *data){
    _data = data;
}

void MorphologicalTransformation::set_golay(int golay){
    _golay = golay;
}

void MorphologicalTransformation::start(){
    _thread = new MorphThread(_data);
    _thread->set_golay(_golay);

    connect(_thread, SIGNAL(finished_signal(long*)), this, SLOT(finished_slot(long*)));
    connect(_thread, SIGNAL(log_signal(QString)), this, SLOT(thread_log_slot(QString)));
    connect(_thread, SIGNAL(error_signal(QString)), this, SLOT(thread_error_slot(QString)));
    connect(_thread, SIGNAL(progress_signal(int)), this, SLOT(thread_progress_slot(int)));
    connect(_thread, SIGNAL(update_image_signal(long*)), this, SLOT(update_image_slot(long*)));

    _thread->start();
    if(_thread->isFinished()){
        delete(_thread);
    }
}

void MorphologicalTransformation::stop_thread(){
    if(_thread != nullptr && _thread->isRunning()){
        _thread->terminate();
        emit log_signal(QString("Výpočetní vlákno zastaveno."));
        emit thread_progress_signal(0);
    }
}

void MorphologicalTransformation::update_image_slot(long * image){
    QImage img = Matrix::create_image(image, _data->get_image_width(), _data->get_image_height(), _data->get_fg_color());
    _image = img;
    emit image_signal(img);
}

void MorphologicalTransformation::finished_slot(long *result){
    update_image_slot(result);
    delete(result);
    emit thread_progress_signal(0);
}

QImage MorphologicalTransformation::get_image(){
    return _image;
}

void MorphologicalTransformation::print_byte(uint16_t byte){
    std::bitset<16> y(byte);
    std::cout << y << std::endl;
}

void MorphologicalTransformation::thread_progress_slot(int progress){
    int value = ((double)progress / (double)_data->get_max_iters()) * 100;
    emit thread_progress_signal(value);
}

void MorphologicalTransformation::thread_log_slot(QString data){
    emit log_signal(data);
}

void MorphologicalTransformation::thread_error_slot(QString data){
    emit error_signal(data);
}

