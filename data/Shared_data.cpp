//
// Created by fifal on 26.2.18.
//

#include "Shared_data.h"

Shared_data::Shared_data() {
    _image_matrix = nullptr;
    _threshold = 127; // default threshold
    _max_iters = 200; // default maximum number of iterations

    _settings_changed = true; // if set to true, get_image_matrix will generate new matrix
}

/**
 * Creates image matrix from input image
 *
 * @brief Shared_data::create_image_matrix
 */
void Shared_data::create_image_matrix(){
    _image_matrix = new long[_image.height() * _image.width()];
    memset(_image_matrix, 0, sizeof(long) * (_image.height() * _image.width()));

    for (int i = 0; i < _image.width(); i++) {
        for (int j = 0; j < _image.height(); j++) {
            auto color = _image.pixelColor(i, j);
            auto r = color.red();
            auto g = color.green();
            auto b = color.blue();

            int gray = (r+g+b) / 3;

            if (gray <=  (255 - _threshold))
                _fg_color == 0 ? set_on_index(j, i, 1) : set_on_index(j, i, 0);
            else
                _fg_color == 0 ? set_on_index(j, i, 0) : set_on_index(j, i, 1);
        }
    }
}

// GETTERS AND SETTERS
/**
 * Sets image file path and loads it into QImage
 *
 * @brief Shared_data::set_image_file
 * @param filename
 */
void Shared_data::set_image_file(QString filename) {
    _filename = filename;
    _image = QImage(filename);

    if (_image.isNull()) {
        emit error_signal("Obrázek je null!");
        return;
    }
    if(_image_matrix != nullptr){
        delete(_image_matrix);
    }
    _image_matrix = nullptr;
}


/**
 * Returns image matrix, if it wasn't created yet or settings has changed
 *      -> recreate matrix
 *
 * @brief Shared_data::get_image_matrix
 * @return
 */
long *Shared_data::get_image_matrix() {
    if(_image_matrix == nullptr || _settings_changed){
        create_image_matrix();
        _settings_changed = false;
    }
    return _image_matrix;
}

long Shared_data::get_on_index(int i, int j) {
    return _image_matrix[i * _image.width() + j];
}

inline void Shared_data::set_on_index(int i, int j, long value) {
    _image_matrix[i * _image.width() + j] = value;
}

void Shared_data::set_threshold(int threshold) {
    _threshold = threshold;
    _settings_changed = true;
}

void Shared_data::set_max_iters(int max_iters){
    _max_iters = max_iters;
}

void Shared_data::set_fg_color(int color){
    _fg_color = color;
    _settings_changed = true;
}

int Shared_data::get_max_iters(){
    return _max_iters;
}

int Shared_data::get_image_width() {
    return _image.width();
}

int Shared_data::get_image_height() {
    return _image.height();
}

QImage Shared_data::get_image(){
    return _image;
}

Shared_data::~Shared_data(){
    delete(_image_matrix);
}

int Shared_data::get_threshold(){
    return _threshold;
}

int Shared_data::get_fg_color(){
    return _fg_color;
}

QString Shared_data::get_image_filename() {
    return _filename;
}

