#ifndef MATRIX_H
#define MATRIX_H

#include<iostream>
#include<QImage>

class Matrix
{
public:

    // Returns number on index (i,j) in 1D matrix
    static long get_on_index(int i, int j, const long* matrix, int matrix_width);
    // Sets value on index (i,j) in 1D matrix
    static void set_on_index(int i, int j, long value, long* matrix, int matrix_width);
    // Returns true if matrix are equal, false otherwise
    static bool are_equal(const long* matrix_a, const long* matrix_b, int matrix_width, int matrix_height);
    // Prints matrix in console
    static void print_matrix(const long* matrix, int matrix_width, int matrix_height);

    // Creates image from matrix
    static QImage create_image(const long* matrix, int image_width, int image_height, int fg_color);
    // Not used, creates distance transformation image, just for debugging
    static QImage create_image_dist(const long* matrix, int image_width, int image_height, int fg_color);


private:
    Matrix();
};

#endif // MATRIX_H
