//
// Created by fifal on 26.2.18.
//

#ifndef ZVI_SHARED_DATA_H
#define ZVI_SHARED_DATA_H

#include <iostream>
#include <QtCore/QString>
#include <QtGui/QImage>

class Shared_data : public QObject{
    Q_OBJECT
private:
    QString _filename;
    QImage _image;
    long *_image_matrix;

    // Image threshold
    int _threshold;
    // Max iterations allowed
    int _max_iters;
    // If settings were changed = true
    bool _settings_changed;
    // Foreground color, 0 = black, 1 = white
    int _fg_color;

    // 1D array as 2D array utility methods
    // returns value on matrix[i][j]
    long get_on_index(int i, int j);
    // sets value on matrix[i][j]
    void set_on_index(int i, int j, long value);

    // Generates new image matrix
    void create_image_matrix();

signals:
    // Logging signal which calls log in mainwindow.cpp
    void log_signal(QString data);
    // Error signal which calls error in mainwindow.cpp
    void error_signal(QString data);

public:
    Shared_data();
    ~Shared_data();

    /// SETTERS

    void set_threshold(int threshold);
    void set_max_iters(int max_iters);
    void set_image_file(QString filename);
    void set_fg_color(int color);

    /// GETTERS

    // Returns max number of allowed iterations
    int get_max_iters();

    // Returns image width
    int get_image_width();
    // Returns image height
    int get_image_height();

    // Returns source image
    QImage get_image();

    // Returns for threshold
    int get_threshold();

    // Returns image matrix array
    long *get_image_matrix();

    // Returns source image filename
    QString get_image_filename();

    // Returns foreground color
    int get_fg_color();
};


#endif //ZVI_SHARED_DATA_H
