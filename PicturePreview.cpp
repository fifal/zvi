#include "PicturePreview.h"
#include "ui_PicturePreview.h"

PicturePreview::PicturePreview(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PicturePreview)
{
    ui->setupUi(this);

    this->setAttribute( Qt::WA_QuitOnClose, false );
    this->setFixedSize(QSize(700,650));

    _gw = new QGraphicsView(this);

    _slider = new QSlider(Qt::Horizontal, this);
    _slider->setMinimum(100);
    _slider->setMaximum(2000);
    _slider->setMinimumWidth(400);
    _slider->setTickPosition(QSlider::TicksBelow);
    _slider->setTickInterval(100);

    connect(_slider, SIGNAL(valueChanged(int)), this, SLOT(value_changed_slot(int)));
}

/**
 * Sets image into windows
 *
 * @brief PicturePreview::set_image
 * @param image
 */
void PicturePreview::set_image(QImage image){
    if(!image.isNull()){
        _image = image;
    }

    auto scene = new QGraphicsScene(this);

    _gw->setMinimumWidth(600);
    _gw->setMinimumHeight(600);

    _gw->setMaximumWidth(600);
    _gw->setMaximumHeight(600);

    QPixmap pixmap(QPixmap::fromImage(_image));
    scene->addPixmap(pixmap);
    _gw->setScene(scene);

    ui->gridLayout->addWidget(_gw,0,0,1,0,Qt::AlignHCenter);
    ui->gridLayout->addWidget(_slider,2,0,3,0,Qt::AlignHCenter);


    this->setLayout(ui->gridLayout);
}

/**
 * Image zooming
 *
 * @brief PicturePreview::value_changed_slot
 * @param value
 */
void PicturePreview::value_changed_slot(int value){
    double scale = value / 100.0;
    _gw->resetMatrix();
    _gw->scale(scale, scale);
}

void PicturePreview::closeEvent(QCloseEvent *bar){
    emit on_close_signal();
}

PicturePreview::~PicturePreview()
{
    delete ui;
}
