#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "data/Shared_data.h"
#include <algorithms/MAS.h>
#include "PicturePreview.h"
#include <algorithms/MorphologicalTransformation.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();
    void on_btnSkelet_clicked();
    void on_btnSave_clicked();
    void on_btnMaxResult_clicked();
    void on_btnStop_clicked();


    void set_delta_distance_slot(int delta);
    void set_delta_skeleton_slot(int delta);
    void set_golay_slot(int golay);
    void set_threshold(int threshold);
    void set_max_iters(int max_iters);
    void set_fg_color(int color);
    void log(QString string);
    void error(QString error);
    void show_image(QImage image);
    void progress_slot(int progress);
    void result_preview_slot(void);


private:
    Shared_data* _data;
    Ui::MainWindow *ui;
    PicturePreview* _pic_preview;
    PicturePreview* _pic_preview_source;
    MAS* _mas;
    MorphologicalTransformation* _mot;

    bool _result_preview_open;
    bool _result_source_open;
};

#endif // MAINWINDOW_H
